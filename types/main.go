package main

import "fmt"

func main() {
	// Integers and floats.
	fmt.Println("NumberOfLightsabers =", 123)
	fmt.Println("ChewbaccaIQ =", 334.0/3.0)
	fmt.Println()

	// Booleans, with boolean operators as you'd expect.
	LukeIamYourFather := true //implicit
	fmt.Println(LukeIamYourFather)
	fmt.Println(true || false)
	fmt.Println(!true)
	fmt.Println()

	if LukeIamYourFather {
		// Strings, which can be added together with `+`.
		fmt.Println("Noooooooooooooooooooooo" + "!!!!!")
	}
	fmt.Println()

	//struct types
	type LightSaber struct {
		Power int
		Color string
	}

	var VaderLightsaber LightSaber //explicit declaration
	VaderLightsaber.Color = "red"
	VaderLightsaber.Power = 100

	fmt.Printf("%T\n", VaderLightsaber)
	fmt.Println(VaderLightsaber)
	fmt.Println()

	//arrays
	fmt.Println("StormTroopers....")
	stormTroopers := [2]byte{}
	for _, trooper := range stormTroopers {
		fmt.Println(trooper)
	}
	fmt.Println()

	//slices
	fmt.Println("Rebels....")
	rebels := []byte{}
	fmt.Println(rebels)
	rebelsExtended := append(rebels, 0)
	rebels = make([]byte, 3)

	fmt.Println(rebelsExtended)
	fmt.Println(rebels)
	fmt.Println()

	//slice from arrays
	fmt.Println("Slice of Stormtroopers")
	var troopers []byte
	troopers = stormTroopers[:]
	fmt.Println(troopers)
	stormTroopers[0] = 123
	fmt.Println(troopers)
	fmt.Println()

	//maps
	fmt.Println("Starring...")
	starring := map[string]string{"Yoda": "Frank Oz", "Luke Skywalker": "Mark Hamill"}
	fmt.Println(starring)
	fmt.Println()

	//pointers
	lordVader := "dark"
	anakinSkywalker := &lordVader
	*anakinSkywalker += " side"
	fmt.Println(lordVader)
	fmt.Println()

	//func types
	chewie := func() {
		fmt.Println("brrwhhhaargh")
	}
	chewie()
	fmt.Println()
}

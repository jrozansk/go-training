package main

import (
	"fmt"
)

type Fighter interface {
	Attack()
}

type Jedi struct {
	LightSaber string
}

type Ewok struct {
	CrossBow string
}

func (j Jedi) Attack() {
	fmt.Println(j.LightSaber)
}

func Fight(fighter Fighter) {
	fighter.Attack()
}

func main() {
	Fight(Jedi{LightSaber: "bzzzzZzz"})

	//io.Reader()
	//strings.NewReader
}

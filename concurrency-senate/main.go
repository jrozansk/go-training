package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	f, err := os.Open("/home/jrozansk/Sources/go-workspace/src/bitbucket.org/jrozansk/go-training/concurrency-senate/votes")
	defer f.Close()
	if err != nil {
		fmt.Println("Reading votes failed", err)
		return
	}

	result := map[string]int{}

	reader := bufio.NewReader(f)
	var line string
	for err != io.EOF {
		line, err = reader.ReadString('\n')
		line = strings.TrimSpace(line)
		result[line] += 1
	}

	fmt.Println(result)
}

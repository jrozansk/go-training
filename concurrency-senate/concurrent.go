package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
)

func count(buffer chan string, result chan map[string]int) {
	count := map[string]int{}
	for {
		s, more := <-buffer

		if !more {
			break
		}

		count[s] += 1
	}
	result <- count
}

func main() {
	fmt.Println("Number of cores used:", runtime.NumCPU())

	f, err := os.Open("/home/jrozansk/Sources/go-workspace/src/bitbucket.org/jrozansk/go-training/concurrency-senate/votes")
	defer f.Close()
	if err != nil {
		fmt.Println("Reading votes failed", err)
		return
	}

	input := make(chan string)
	resultA := make(chan map[string]int)
	resultB := make(chan map[string]int)

	go count(input, resultA)
	go count(input, resultB)

	reader := bufio.NewReader(f)
	go func() {
		var line string
		for err != io.EOF {
			line, err = reader.ReadString('\n')
			line = strings.TrimSpace(line)
			input <- line
		}
		close(input)
	}()

	outA := <-resultA
	outB := <-resultB
	fmt.Println(outA)
	fmt.Println(outB)

	result := map[string]int{}
	for k, v := range outA {
		result[k] += v
	}
	for k, v := range outB {
		result[k] += v
	}
	fmt.Println(result)
}

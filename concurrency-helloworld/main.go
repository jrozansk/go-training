package main

import (
	"fmt"
	"time"
)

type XWing struct {
	number int
}

func Destroy(ship XWing) {
	fmt.Println("Shooting to XWing", ship.number)
}

func main() {
	ships := []XWing{}
	for i := 0; i < 10; i++ {
		ships = append(ships, XWing{number: i})
	}

	for _, ship := range ships {
		go Destroy(ship)
	}

	go fmt.Println("Stay on target")
	time.Sleep(time.Millisecond * 100)
}
